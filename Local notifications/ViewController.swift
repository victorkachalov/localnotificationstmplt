//
//  ViewController.swift
//  Local notifications
//
//  Created by iOS_Razrab on 21/09/2017.
//  Copyright © 2017 iOS_Razrab. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {

    @IBAction func sendNotification(_ sender: UIButton) {
        sheduleNotification(inSeconds: 5) { success in
            
            if success {
                print("Успешно")
            } else {
                print("Потрачено")
            }
            
        }
    }
    
    /*************************  РАБОТА С ЛОКАЛЬНЫМИ УВЕДОМЛЕНИЯМИ *************************/
    //--- для отображения локальных уведомлений, приложение должно быть неактивно (свернуто)
    
    
    func sheduleNotification(inSeconds seconds: TimeInterval, completion: (Bool) -> ()) { // --- start: создание отправка увеломлений через некоторое время
        
        removeNotifitaions(withIdentifiers: ["MyIdentifier"]) // --- нужно для того, чтобы старые уведомления с этим id были удалены и замещены на новые с этим id
        
        let date = Date(timeIntervalSinceNow: seconds) // --- время через которое планируется отправка уведомления
        
        print(Date()) // --- напечатаем текущую дату
        print(date)   // --- напечатаем нашу дату
        
        let content = UNMutableNotificationContent()   // --- содержание нашего уведомления
        content.title = "The notification title"
        content.body  = "The notification body"
        content.sound = UNNotificationSound.default()
        
        let calendar = Calendar(identifier: .gregorian)// --- инициализируем календарь
        let components = calendar.dateComponents([.month, .day, .hour, .minute, .second], from: date) // ---  разбиваем нашу дату на компоненты
        let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false) // --- триггер на отправку уведомлений по дате
        let request = UNNotificationRequest(identifier: "MyIdentifier", content: content, trigger: trigger) // --- запрос на отправку уведомлений
        
        let center = UNUserNotificationCenter.current()// --- обращаемся к центру уведомлений
        center.add(request, withCompletionHandler: nil)// --- с нашим request
        
        
    } // --- end: создание отправка увеломлений через некоторое время
    
    
    func removeNotifitaions(withIdentifiers identifiers: [String]) { // --- start: удаление запроса увеломлений через некоторое время
        
        let center = UNUserNotificationCenter.current()
        center.removePendingNotificationRequests(withIdentifiers: identifiers)
        
    } // --- end: удаление запроса увеломлений через некоторое время
    
    deinit { // --- start: предусматриваем деинициализатор для удаления объекта с уведомлениями
        
        removeNotifitaions(withIdentifiers: ["MyIdentifier"])
        
    } // --- end: предусматриваем деинициализатор для удаления объекта с уведомлениями
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }



}

